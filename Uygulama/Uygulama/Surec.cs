﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Uygulama
{
    public class Surec
    {
        string[] surecList= { };
        int surecNo = 0;
        int surecSonu = 0;
        string anaSurecName = "";
        string gorunmeyecekUrl = null;
        string[] kisitlar = { };
        string[] degerler = { };
        Hashtable egitimVideoUrl = null;
        string anaSurecUrl = null;
        string surecSonuUrl = null;
        string[] aciklamalar = null;
        string[] referanslar = null;
        string[] konfigurasyonlar = null;

        public Surec(string anaSurecName,string[] surecList,string gorunmeyecekUrl,Hashtable egitimVideoUrl)
        {
            this.surecList = surecList;
            this.surecNo = 0;
            this.surecSonu = surecList.Length-1;
            this.anaSurecName = anaSurecName;
            this.gorunmeyecekUrl = gorunmeyecekUrl;
            this.egitimVideoUrl = egitimVideoUrl;
        }
        public void setSurecNo(int surecNo) {
            this.surecNo = surecNo;
        }
        public void setKisitlar(string[] kisitlar) {
            this.kisitlar = kisitlar;
        }
        public string[] getKisitlar() {
            return this.kisitlar;
        }
        public void setDegerler(string[] degerler)
        {
            this.degerler = degerler;
        }
        public string[] getDegerler()
        {
            return this.degerler;
        }
        public void setReferanslar(string[] referanslar)
        {
            this.referanslar = referanslar;
        }
        public string[] getReferanslar()
        {
            return this.referanslar;
        }
        public void setKonfigurasyonlar(string[] konfigurasyonlar)
        {
            this.konfigurasyonlar = konfigurasyonlar;
        }
        public string[] getKonfigurasyonlar()
        {
            return this.konfigurasyonlar;
        }
        public void setAnaSurecUrl(string url)
        {
            this.anaSurecUrl = url;
        }
        public string getAnaSurecUrl()
        {
            return this.anaSurecUrl;
        }
        public void setSurecSonuUrl(string url)
        {
            this.surecSonuUrl = Path.Combine(url.TrimStart().TrimEnd());
        }
        public string getSurecSonuUrl()
        {
            return this.surecSonuUrl;
        }
        public void setAciklamalar(string[] str)
        {
            this.aciklamalar =str ;
        }
        public string[] getAciklamalar()
        {
            return this.aciklamalar;
        }


        public string oncekiSurec(int index) {

            if (index != 0)
            {
                this.surecNo = index - 1;
                return getSurec();
            }
            else
            {
                Form_Alert msg = new Form_Alert();
                msg.showAlert("Daha Önde Bir Süreç Bulunamamıştır", Form_Alert.enmType.Warning);
                return null;
            }
        }
        public string sonrakiSurec(int index)
        {

           if (index < surecSonu)
            {
                this.surecNo = index + 1;
                return getSurec();
            }
            else
            {
                Form_Alert msg = new Form_Alert();
                msg.showAlert("Daha Önde Bir Süreç Bulunamamıştır", Form_Alert.enmType.Warning);
                return null;
            }

        }
        public string getSurec() {
            return Path.Combine(Environment.CurrentDirectory, @"Süreç\", surecList[surecNo]); 
        }
        public string getSurecGorunmeyecek()
        {
            if (gorunmeyecekUrl != null)
                return Path.Combine(Environment.CurrentDirectory, @"Süreç\", gorunmeyecekUrl);
            else
                return null;
        }
        public string getSurecUrlForParametre(String url) { 
        return Path.Combine(Environment.CurrentDirectory, @"Süreç\", url);
        }
        public string getVideoUrlLink(string key)
        {

            return (string)egitimVideoUrl[key];
        }
        public int getSurecNo()
        {
            return surecNo;
        }
        public string getSurecName() {
            string[] nl = surecList[surecNo].Replace(".txt", "").Split("\\");
            return nl[nl.Length-1];
        }
        public string getSurecListName() {
            return surecList[surecNo];
        }
        public string getAnaSurecName()
        {
            return this.anaSurecName;
        }
        public int getSurecSonu()
        {

            return surecSonu;
        }


        public string[] sureciSonlandir(string url)
        {
            DosyaPath dosya = new DosyaPath();
            List<string> list = new List<string>();
            string[] kList = dosya.txtVeriOku(url);

            foreach (string s in surecList)
            {
                foreach (string v in dosya.txtVeriOku(Path.Combine(Environment.CurrentDirectory, @"Süreç\", s)))
                    list.Add(v);
            }


            for (int i = 0; i < kList.Length; i++)
                foreach (string t in list.ToArray())
                    if (kList[i].Split("=")[0].TrimEnd().TrimStart().Equals(t.Split("=")[0].TrimEnd().TrimStart()))
                    {
                        string value = t.Split("=")[1].Replace(",", ".");
                        string[] d = kList[i].Split("=");

                        kList[i] = d[0].TrimEnd().TrimStart() + " = " + value;
                    }


            //setSurecNo(0);
            return kList;
        }

        public string[] sureciSonlandir() {
            DosyaPath dosya = new DosyaPath();
            List<string> list = new List<string>();
            string[] kList = dosya.txtVeriOku(getSurecSonuUrl());

            foreach (string s in surecList)
            {
                foreach (string v in dosya.txtVeriOku(Path.Combine(Environment.CurrentDirectory, @"Süreç\", s)))
                    list.Add(v);
            }

            
                for (int i=0;i<kList.Length;i++)
                foreach(string t in list.ToArray())
                    if (kList[i].Split("=")[0].TrimEnd().TrimStart().Equals(t.Split("=")[0].TrimEnd().TrimStart()))
                    {
                        string value = t.Split("=")[1].Replace(",",".");
                        string[] d = kList[i].Split("=");

                        kList[i]=d[0].TrimEnd().TrimStart() + " = " + value;
                    }


            //setSurecNo(0);
            return kList;
        }

        public string[] sureciSonlandir(string [] kList,string[] list)
        {
           for (int i = 0; i < kList.Length; i++)
                foreach (string t in list)
                    if (kList[i].Split("=")[0].TrimEnd().TrimStart().Equals(t.Split("=")[0].TrimEnd().TrimStart()))
                    {
                        string value = t.Split("=")[1];
                        string[] d = kList[i].Split("=");

                        kList[i] = d[0].TrimEnd().TrimStart() + " = " + value;
                    }


            //setSurecNo(0);
            return kList;
        }


    }
}
