﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Uygulama
{
    class DosyaPath
    {
        
        String veriPath= Path.Combine(Environment.CurrentDirectory, @"Data\", "path.txt");
        String surecPath = Path.Combine(Environment.CurrentDirectory, @"Data\", "surec.txt");
        String menuPath = Path.Combine(Environment.CurrentDirectory, @"Data\", "Menu.txt");
        String konfPath = Path.Combine(Environment.CurrentDirectory, @"Data\", "konfigurasyonSurec.txt");
        String configPath = Path.Combine(Environment.CurrentDirectory, @"Data\", "config.properties.txt");
        public DosyaPath() { 
        
        }
        public String getVeriPath() {

            return veriPath;
        }
        public String getSurecPath()
        {
            return surecPath;
        }
        public String getMenuPath()
        {
            return menuPath;
        }
        public String getKonfPath()
        {
            return konfPath;
        }
        public String getKonfDataPath(string surecName)
        {
            return Path.Combine(Environment.CurrentDirectory, @"Data\KONFİGÜRASYONLAR", surecName + ".txt"); ;
        }
        public String getKonfSurecPath(string surecName)
        {
            return Path.Combine(Environment.CurrentDirectory, @"Süreç\KONFİGÜRASYONLAR", surecName + ".txt"); ;
        }
        public String getConfigPath()
        {
            return configPath;
        }
        public String getMenuSurecPath(string surecName) { 
        return Path.Combine(Environment.CurrentDirectory, @"Data\", surecName+".txt");
        }
        public string[] txtVeriOku(String path)
        {
            string[] a = { };
            if (path!=null && path!="")
            { 
         
            try
            {
                   
                    a = File.ReadAllLines(path, System.Text.Encoding.UTF8);
            }
            catch (Exception e) {
                Form_Alert msg = new Form_Alert();
                msg.showAlert(e.Message,Form_Alert.enmType.Warning);
                return a;
            }
            }
            return a;
        }

        public void txtVeriYazNotAlert(String path, string[] veri)
        {
            try
            {
                if (veri != null)
                {

                    File.WriteAllLines(path, veri, System.Text.Encoding.UTF8);
                }


            }
            catch (Exception e)
            {
                Form_Alert msg = new Form_Alert();
                msg.showAlert(e.Message, Form_Alert.enmType.Error);

            }

        }

            public void txtVeriYaz(String path, string[] veri)
        {
            try
            {
                if (veri != null)
                {
                   
                    File.WriteAllLines(path, veri, System.Text.Encoding.UTF8);
                }
                    
                Form_Alert msg = new Form_Alert();
                
                msg.showAlert("Veriler Kaydedildi", Form_Alert.enmType.Success);
            }
            catch (Exception e) {
                Form_Alert msg = new Form_Alert();
                msg.showAlert(e.Message, Form_Alert.enmType.Error);

            }


        }

        public void txtVeriYaz(String path, string[] veri,bool alert)
        {
            try
            {
                if (veri != null)
                {

                    File.WriteAllLines(path, veri, System.Text.Encoding.UTF8);
                }
                if(alert)
                { 
                Form_Alert msg = new Form_Alert();

                msg.showAlert("Veriler Kaydedildi", Form_Alert.enmType.Success);
                }
            }
            catch (Exception e)
            {
                Form_Alert msg = new Form_Alert();
                msg.showAlert(e.Message, Form_Alert.enmType.Error);

            }


        }



    }
}
