﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Uygulama
{
    public partial class Hakkinda : Form
    {
        LisansInformation lisans;
        public Hakkinda(LisansInformation lisans)
        {
            this.lisans = lisans;
            InitializeComponent();
            lisanaBilgiEkle();
        }
        private void lisanaBilgiEkle() {
            lbl_musteri.Text = lisans.getLisansMusteri();
            lbl_tarih.Text = lisans.getTarih().ToString();
            if (lisans.getGecerli())
            {
                lbl_gecerlilik.Text = "Geçerli";
            }
            else
            {
                lbl_gecerlilik.Text = "Geçerli Değil";
                lbl_gecerlilik.ForeColor = Color.Red;
            }
            
        }
        
    }
}
