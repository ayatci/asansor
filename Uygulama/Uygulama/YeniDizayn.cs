﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace Uygulama
{
    public partial class YeniDizayn : Form
    {
        DosyaPath dosya = new DosyaPath();
        string exeyolu = "";
        string lisansYolu = "";
        Surec aktifSurec = null;
        List<Surec> listSurec = new List<Surec>();
        List<Konfigurasyonlar> listKonf = new List<Konfigurasyonlar>();
        List<Panel> subPanelList = new List<Panel>();
        List<Button> subButtonList = new List<Button>();
        LisansInformation lisans = new LisansInformation();
        LisansDogrula lD = new LisansDogrula();
        public YeniDizayn()
        {
            InitializeComponent();

            hideSubMenu();
            dwgPathGetir();
            lisans = lD.lisansDogrula(lisansYolu);
            if (lisans.getGecerli())
                menuGetir();
            else
                alertLisans();
            // konfMenuOlustur(dosya.txtVeriOku(dosya.getKonfPath()));
            this.check();

            //konfListGetir();

        }

        public void alertLisans() {
            Alert("Lisans Kontrolü Başarısız. Lütfen Ayarlardan Lisans Yolunu Kaydediniz ve Yeniden başlatınız.", Form_Alert.enmType.Info);
        }
        public List<Konfigurasyonlar> konfListGetir(string name) {

            string[] konfStr = dosya.txtVeriOku(dosya.getKonfDataPath(name));
            listKonf.Clear();
            foreach (string s in konfStr)
            {
                Konfigurasyonlar item = new Konfigurasyonlar();
                string[] dtos = s.Split(";");
                item.setKodu(dtos[0]);
                item.setUstKodu(dtos[1]);
                item.setAdi(dtos[2]);
                listKonf.Add(item);
            
            }
            return listKonf;
    }
        private void menuGetir()
        {


            MenuOlustur(dosya.txtVeriOku(dosya.getMenuPath()));
        }
        public void Alert(string msg, Form_Alert.enmType type)
        {
            Form_Alert frm = new Form_Alert(); //her çağırılmada Form_alert nesnesi oluşturmak doğru değildir.
            frm.showAlert(msg, type);
        }
        private void dwgPathGetir()
        {
            string[] cList = dosya.txtVeriOku(dosya.getConfigPath());
            foreach (string m in cList)
            {
                string[] k = m.Split("=");
                if (k[0].Split("-")[1].Trim() == "dwgUygulamaYolu")
                    exeyolu = k[1].TrimStart().TrimEnd();
                if (k[0].Split("-")[1].Trim() == "lisansDosyaYolu")
                    lisansYolu = k[1].TrimStart().TrimEnd();
            }
        }

        public void konfMenuOlustur(string[] list) {
            Button btn;
            Panel panel = new Panel();

            subMenuPanelDizayn(panel, "panelKonfSubMenu");
            subPanelList.Add(panel);
            for (int i = list.Length - 1; i >= 0; i--)
            {
                string[] str = list[i].Split("=");
               
             

                if (str.Length > 0)
                {
                    
                 
                    btn = new Button();
                    subButtonList.Add(btn);
                    menuButtonDizayn(panel, btn, str[0], str[1].Trim(), 20,true);
                    btn.Click += new System.EventHandler(this.btnKonfigurasyuonSubMenu_Click);
                  
                  

                }


           
            }
            this.panelKonf.Controls.Add(panel);
            btn = new Button();

            btn.Click += new System.EventHandler(this.btnKonfigurasyonlar_Click);
            menuButtonDizayn(this.panelKonf, btn, "KONFİGÜRASYONLAR", "Konfigurasyonlar", 1,false);

        }
        public void MenuOlustur(string[] list)
        {

            Button btn;
            for (int i = list.Length - 1; i >= 0; i--)
            {
                string[] str = list[i].Split("=");
                string[] menuList = { };
                string anaSurecUrl = null;
                Hashtable egitimLinkler = new Hashtable();
                string txtGorunmeyecekUrl = null;

                try
                {
                    menuList = dosya.txtVeriOku(dosya.getMenuSurecPath(str[1].Trim()));
                }
                catch (Exception msg)
                {
                    this.Alert(msg.Message, Form_Alert.enmType.Error);
                }
                string[] srcList = { };
                if (menuList.Length > 0)
                {
                    Panel panel = new Panel();

                    subMenuPanelDizayn(panel, "panel" + str[1].Trim() + "SubMenu");
                    subPanelList.Add(panel);
                    for (int j = menuList.Length - 1; j >= 0; j--)
                    {
                        // srcList[j] = menuList[j].Split("=")[0];
                        btn = new Button();
                        string[] split = menuList[j].Split(";");
                        if (split.Length > 1)
                            egitimLinkler[split[0]] = split[1];
                        else
                            egitimLinkler[split[0]] = null;
                        menuList[j] = menuList[j].Split(";")[0];
                        string[] nl = menuList[j].Replace(".txt", "").Split("\\");
                        txtGorunmeyecekUrl = menuList[j].Replace(".txt", "").Split("\\")[0] + "\\" + menuList[j].Replace(".txt", "").Split("\\")[0] + " GÖRÜNMEYECEKLER.txt";
                        // kisitlarUrl = menuList[j].Replace(".txt", "").Split("\\")[0] + "\\" + menuList[j].Replace(".txt", "").Split("\\")[0] + " KISITLAR.txt";
                        anaSurecUrl = menuList[j].Replace(".txt", "").Split("\\")[0];
                        menuButtonDizayn(panel, btn, nl[nl.Length - 1], str[1].Trim() + "__" + nl[nl.Length - 1] + "__" + j, 20,true);
                        btn.Click += new System.EventHandler(this.btnSurecSubMenu_Click);
                        subButtonList.Add(btn);

                    }
                    this.panelSideMenu2.Controls.Add(panel);

                }


                btn = new Button();

                btn.Click += new System.EventHandler(this.btnSideMenu2_Click);
                menuButtonDizayn(this.panelSideMenu2, btn, str[0].Trim(), str[1].Trim(), 1,false);
                Surec src = new Surec(btn.Name, menuList, txtGorunmeyecekUrl, egitimLinkler);
                src.setAnaSurecUrl(anaSurecUrl);
                src.setKisitlar(dosya.txtVeriOku(src.getSurecUrlForParametre(src.getAnaSurecUrl() + "\\" + src.getAnaSurecUrl() + " KISITLAR.txt")));
                src.setDegerler(dosya.txtVeriOku(src.getSurecUrlForParametre(src.getAnaSurecUrl() + "\\" + src.getAnaSurecUrl() + " DEĞERLER.txt")));
                src.setAciklamalar(dosya.txtVeriOku(src.getSurecUrlForParametre(src.getAnaSurecUrl() + "\\" + src.getAnaSurecUrl() + " AÇIKLAMALAR.txt")));
                src.setReferanslar(dosya.txtVeriOku(src.getSurecUrlForParametre(src.getAnaSurecUrl() + "\\" + src.getAnaSurecUrl() + " REFERANSLAR.txt")));
                src.setKonfigurasyonlar(dosya.txtVeriOku(src.getSurecUrlForParametre(src.getAnaSurecUrl() + "\\" + src.getAnaSurecUrl() + " KONFİGÜRASYONLAR.txt")));
                src.setSurecSonuUrl(str.Length > 2 ? str[2] : null);
                listSurec.Add(src);
            }


        }

        public void menuButtonDizayn(Panel panel, Button btn, string txt, string name, int left,Boolean subButton)
        {

            panel.Controls.Add(btn);
            btn.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            btn.FlatAppearance.BorderSize = 0;
            btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            if (subButton)
            btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            btn.Name = name;
            btn.Padding = new System.Windows.Forms.Padding(left, 0, 0, 0);
            btn.Size = new System.Drawing.Size(200, 45);
            btn.TabIndex = 3;
            btn.Text = txt;
            btn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            btn.UseVisualStyleBackColor = true;
            btn.Dock = System.Windows.Forms.DockStyle.Top;



        }
        public void subMenuPanelDizayn(Panel pnl, string name)
        {
            pnl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            pnl.Dock = System.Windows.Forms.DockStyle.Top;
            pnl.Location = new System.Drawing.Point(0, 122);
            pnl.Name = name;
            pnl.Size = new System.Drawing.Size(200, 123);
            pnl.TabIndex = 2;
            pnl.AutoSize = true;
            pnl.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            pnl.Visible = false;
        }


        private void hideSubMenu()
        {

            foreach (Panel p in subPanelList)
            {
                p.Visible = false;
            }
        }

        private void showSubMenu(Panel subMenu)
        {
            if (subMenu.Visible == false)
            {
                hideSubMenu();
                subMenu.Visible = true;
            }
            else
                subMenu.Visible = false;
        }



        private void btnSideMenu2_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            Panel subMenu = this.panelSideMenu2.Controls["panel" + btn.Name + "SubMenu"] as Panel;
            if (subMenu != null)
                showSubMenu(subMenu);
        }
        private void btnSurecSubMenu_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string[] names = btn.Name.Split("__");
            foreach (Surec s in this.listSurec)
            {
                if (s.getAnaSurecName().Equals(names[0]))
                {

                    butonRenklendir(btn);
                    s.setSurecNo(Int32.Parse(names[2]));
                    aktifSurec = s;
                    panelVeriEkle(s.getSurec(), s.getSurecGorunmeyecek(), s.getVideoUrlLink(s.getSurecListName()),true);


                }

            }


        }
        public void degerKaydet(string[] lines,bool delete) {
            string line = "";
            string sonStr = "";
            string key = lines[0].Split("=")[1].Replace("\"", "").Trim();
            string degerAdi = lines[0].Split("=")[0].Replace("\"", "").Trim();
            foreach (string deger in lines)
            {
             
                if (!deger.Split("=")[0].Replace("\"", "").Trim().Equals(degerAdi.Trim()))
                {
                    if (!line.Equals(""))
                        line = line + "-";
                   
                    line = line + deger.Split("=")[0].TrimStart().TrimEnd().Replace("\"", "") + ":" + deger.Split("=")[1].TrimStart().TrimEnd();
                }
              
                   
            }
            line = key + ";" + line;
                string[] degerler = aktifSurec.getDegerler();
            int j = 0;
            foreach (string str in degerler) {
                bool kontrol = true;
                string kStr = str.Replace("{", "").Replace("}", "");
                string surecDegerAdi = kStr.Trim().Split("=")[0].TrimStart().TrimEnd();
                
                if(degerAdi.Equals(surecDegerAdi))
                { 
                string[] items = kStr.Trim().Split("=")[1].Split(",");
               
                for (int i = 0; i < items.Length; i++)
                {
                    if (items.Length > 0 && items[i].Contains("-"))
                    {
                        string[] value = items[i].Split(";");
                        string[] maps = value[1].Split("-");

                        if (value[0].Trim().Equals(key.Trim()))

                        {
                            if(!delete)
                                sonStr = sonStr + (sonStr.Equals("") ? "" : ",") + line;
                            kontrol = false;
                        }
                           
                        else
                            sonStr = sonStr + (sonStr.Equals("") ? "" : ",") + items[i];
                        


                    }
                  
                }
                if(kontrol)
                    sonStr = sonStr + (sonStr.Equals("") ? "" : ",") + line;
                sonStr =degerAdi+ "= {" + sonStr + "}";
                    degerler[j] = sonStr;

                }
                j++;
            }
            aktifSurec.setDegerler(degerler);

            dosya.txtVeriYaz(aktifSurec.getSurecUrlForParametre(aktifSurec.getAnaSurecUrl() + "\\" + aktifSurec.getAnaSurecUrl() + " DEĞERLER.txt"),aktifSurec.getDegerler());

         panelVeriEkle(aktifSurec.getSurec(), aktifSurec.getSurecGorunmeyecek(), aktifSurec.getVideoUrlLink(aktifSurec.getSurecListName()), true);

        }
        private void btnKonfigurasyuonSubMenu_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            
            panelVeri.Controls.Clear();

            Konfigurasyon frm = new Konfigurasyon();
            frm.setKonfigurasyonList(konfListGetir(btn.Name));
            frm.setSurecName(btn.Name);
           
            frm.verileriOkuSetle();
            frm.setVeris();
            openChildFormInPanel(frm);




        }

        private void butonRenklendir(Button btn)
        {
            foreach (Button b in subButtonList)
            {
                b.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            }
            btn.ForeColor = Color.Gold;

        }

        public void panelVeriEkle(string url, string urlVeriKontrol, string videoLink,bool veriPage)
        {


            // Alert("Listeleme Başarılı", Form_Alert.enmType.Listeleme_Başarılı);
            VeriDuzenle veri = new VeriDuzenle(lisans);
            veri.setVeriPage(veriPage);
            panelVeri.Controls.Remove(veri);
            veri.setLabelBaslik(aktifSurec.getSurec().Substring(aktifSurec.getSurec().LastIndexOf("\\") + 1).Replace(".txt", ""));
            //   veri.TopLevel = false;
            veri.setVideoUrl(videoLink != null ? videoLink.Trim() : null);
            try
            {
                veri.verileriDosyadanAl(dosya.txtVeriOku(url), dosya.txtVeriOku(urlVeriKontrol), aktifSurec);
            }
            catch (Exception msg)
            {
                this.Alert(msg.Message, Form_Alert.enmType.Error);
            }

            openChildFormInPanel(veri);
        }

        public void konfigurasyonVeriEkle(string url, string urlVeriKontrol)
        {


            // Alert("Listeleme Başarılı", Form_Alert.enmType.Listeleme_Başarılı);
            Konfigurasyon konf = new Konfigurasyon();
            panelVeri.Controls.Remove(konf);
            //veri.setLabelBaslik(aktifSurec.getSurec().Substring(aktifSurec.getSurec().LastIndexOf("\\") + 1).Replace(".txt", ""));
            //   veri.TopLevel = false;
           
            try
            {
               // konf.verileriDosyadanAl(dosya.txtVeriOku(url), dosya.txtVeriOku(urlVeriKontrol), aktifSurec);
            }
            catch (Exception msg)
            {
                this.Alert(msg.Message, Form_Alert.enmType.Error);
            }

            openChildFormInPanel(konf);
        }

        public void panelVeriAlKaydet()
        {
            VeriDuzenle veri = panelVeri.Controls["veriDuzenle"] as VeriDuzenle;
            string[] arrLine = veri.verileriEkrandanAl(dosya.txtVeriOku(aktifSurec.getSurec()));
            if (aktifSurec.getKisitlar().Length > 0)
            {
                foreach (string vL in arrLine)
                {
                    foreach (string kisit in aktifSurec.getKisitlar())
                    {
                        string[] k = kisit.Split(";");
                        string[] v = vL.Split("=");
                        if (k[0] != null && k[0].Replace("\"", "").TrimStart().TrimEnd().Equals(v[0].Split(";")[0].Replace("\"", "").TrimStart().TrimEnd()))
                        {
                            float max = 0, min = 0, value = 0;
                            value = float.Parse(v[1].Replace(",", "."), CultureInfo.InvariantCulture);
                            if (k.Length > 1 && k[1] != null && k[1].Trim() != "")
                                min = getMaxMinVeri(arrLine, k[1]);
                            if (k.Length > 2 && k[2] != null && k[2].Trim() != "")
                                max = getMaxMinVeri(arrLine, k[2]);
                            if (min != 0 && value < min)
                            {

                                this.Alert(k[0] + " değeri " + min + " değerinden küçük olamaz.!!!", Form_Alert.enmType.Error);
                                TextBox t = veri.Controls[v[0].Split(";")[0].Replace("\"", "").TrimStart().TrimEnd()] as TextBox;
                                t.Focus();
                                
                                return;
                            }
                            else if (max != 0 && value > max)
                            {
                                this.Alert(k[0] + " değeri " + max + " değerinden büyük olamaz.!!!", Form_Alert.enmType.Error);
                                TextBox t = veri.Controls[v[0].Split(";")[0].Replace("\"", "").TrimStart().TrimEnd()] as TextBox;
                                t.Focus();
                                veri.ActiveControl = t;

                                return;



                            }
                        }
                    }

                }

            
            }
            dosya.txtVeriYaz(aktifSurec.getSurec(), arrLine);
            if (aktifSurec.getReferanslar().Length > 0)
            {
                foreach (string referans in aktifSurec.getReferanslar())
                {

                    dosya.txtVeriYaz(aktifSurec.getSurecUrlForParametre(referans), aktifSurec.sureciSonlandir(dosya.txtVeriOku(aktifSurec.getSurecUrlForParametre(referans)), arrLine), false);
                }

            }

        }
        public void sureciSonlandir()
        {
            this.check();

            dosya.txtVeriYaz(aktifSurec.getSurecSonuUrl(), aktifSurec.sureciSonlandir());
        }

        public float getMaxMinVeri(string[] veriler, string value)
        {
           
            try
            {
                return float.Parse(value.Replace(",", "."), CultureInfo.InvariantCulture);

            }
            catch
            {

                if (value.Contains("\\"))
                {
                    return getMaxMinVeri(dosya.txtVeriOku(aktifSurec.getSurecUrlForParametre(aktifSurec.getAnaSurecUrl() + "\\" + value.Split("\\")[0] + ".txt")), value.Split("\\")[1]);
                }
                else
                {
                    foreach (string v in veriler)
                        if (v.Split("=")[0].Replace("\"", "").TrimStart().TrimEnd().Equals(value.Replace("\"", "").TrimStart().TrimEnd()))
                            return float.Parse(v.Split("=")[1].Replace(",", "."), CultureInfo.InvariantCulture);
                }
            }
            return 0;

        }

        private Form activeForm = null;
        private void openChildFormInPanel(Form childForm)
        {
            if (activeForm != null)
                activeForm.Close();
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            panelVeri.Controls.Add(childForm);
            panelVeri.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }

        private void btnAyarlar_Click(object sender, EventArgs e)
        {
            panelVeri.Controls.Clear();

            Ayarlar frm = new Ayarlar(lisans);
            openChildFormInPanel(frm);
        }
        FileInfo[] files= new FileInfo[] { };

        private void btnSolide_Butun_Degerleri_Aktar_Click(object sender, EventArgs e)
        {
            if (lisans.getGecerli())
            {
               // btnSolideAktar.BackColor = System.Drawing.Color.;
                btnSolideAktar.Enabled = false;
                panelVeri.Controls.Clear();
                foreach (Surec s in listSurec)
                {
                   //Assuming Test is your Folder
                    this.getDirectoryFiles(files,s.getSurecSonuUrl(),s); 

                   }
                btnSolideAktar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
                btnSolideAktar.Enabled = true;
               
            }
            else
                alertLisans();
            Form_Alert msg = new Form_Alert();

            msg.showAlert("Veriler Kaydedildi", Form_Alert.enmType.Success);

        }
        public void setSureciYaz(FileInfo[] files,Surec s) {
            foreach (var file in files)
            {
                dosya.txtVeriYazNotAlert(file.FullName, s.sureciSonlandir(file.FullName));

            }

        }
        public void getDirectoryFiles(FileInfo[] files,string url,Surec s) {
             
            DirectoryInfo d = new DirectoryInfo(url);
            if (d.GetDirectories().Length > 0)
            {
                foreach (DirectoryInfo dir in d.GetDirectories())
                   this.getDirectoryFiles(files, dir.FullName,s);
            }
                      
           this.setSureciYaz(d.GetFiles("*.txt"),s);
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);

            if (e.CloseReason == CloseReason.WindowsShutDown) return;

            // Confirm user wants to close
            switch (MessageBox.Show(this, "Uygulamayı Kapatmak İstediğinizden Eminmisiniz?", "Kapat", MessageBoxButtons.YesNo))
            {
                case DialogResult.No:
                    e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        [DllImport("user32.dll")]
        static extern IntPtr SetParent(IntPtr child, IntPtr newParent);
        [DllImport("user32.dll")]
        static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool IsWindowVisible(IntPtr hWnd);

        private const int WM_SYSCOMMAND = 274;
        private const int SC_MAXIMIZE = 61488;
        private void btnDwg_Click(object sender, EventArgs e)
        {
            // string exeyolu = "C:\\Users\\YTC\\Desktop\\ASA Yazılım\\ASA v1.3.exe";

            if (lisans.getGecerli())
                if (exeyolu != null && exeyolu != "" && exeyolu.EndsWith(".exe"))
                {
                    try
                    {
                        Process calistir = Process.Start(exeyolu);
                        //this.WindowState = FormWindowState.Minimized;
                        //while (!calistir.HasExited)
                        //{
                            //update UI
 //                       }
   //                     this.WindowState = FormWindowState.Normal;
                    }
                    catch {
                        this.Alert("Uygulama çalıştırılırken hata oluştu. Lütfen uygulama yolunun doğru olduğundan emin olunuz..",Form_Alert.enmType.Error);
                    }
                }
                else
                    this.Alert("Lütfen Bir Exe Dosyası Seçiniz", Form_Alert.enmType.Error);
            else
                alertLisans();
            /*  
              Process calistir = Process.Start(exeyolu);

              while (calistir.MainWindowHandle == IntPtr.Zero || !IsWindowVisible(calistir.MainWindowHandle))
              {
                  System.Threading.Thread.Sleep(10);
                  calistir.Refresh();
              }

              calistir.WaitForInputIdle();
              SetParent(calistir.MainWindowHandle, this.panelVeri.Handle);
              SendMessage(calistir.MainWindowHandle, WM_SYSCOMMAND, SC_MAXIMIZE, 0);
              Process.Start("");
          }
            
            Assembly asm = Assembly.LoadFile(exeyolu);
            Type typez = asm.GetType("asansorProjectv7_Sharp.Form1");
            Form frm = asm.CreateInstance(typez.FullName) as Form;
            if (frm != null)
            {
                openChildFormInPanel(frm);
            }
            */

        }

        private void btnKonfigurasyonlar_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            Panel subMenu = this.panelKonf.Controls["panelKonfSubMenu"] as Panel;
            if (subMenu != null)
                showSubMenu(subMenu);
        }

        private void btnHakkinda_Click(object sender, EventArgs e)
        {
            this.check();
            panelVeri.Controls.Clear();

            Hakkinda frm = new Hakkinda(lisans);
            openChildFormInPanel(frm);
        }

        private void check()
        {

            
            string serial = lisansYolu == null || lisansYolu.Equals("") ? null : lD.GetUSBSerialNumber().getSerialNumberFromDriveLetter(lisansYolu);
            string[] serialGelen = dosya.txtVeriOku(lisansYolu);
            string cozulmus = serialGelen.Length > 0 ? lD.GetLisans().Decrpyt(serialGelen[0], lD.getKey(),lD.getIv()) : null;
            string[] keys = cozulmus != null ? cozulmus.Split("&") : null;
            if (keys != null && keys.Length > 0)
            {
                string cozulmusKey = keys[0];
             
                if (!serial.Equals(cozulmusKey))
                {
                    lisans.setGecerli(false);
                    if (lisans.getGecerli())
                        throw new Exception("Lisans Hatası");
                }
                else
                {
                    lisans.setGecerli(true);
                }
            }
            else
            {
                lisans.setGecerli(false);
                if (lisans.getGecerli())
                    throw new Exception("Lisans Hatası");
            }
        }
    }

   
}
