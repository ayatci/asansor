﻿
using System;
using System.Diagnostics;
using System.Drawing;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Uygulama
{

    
    public partial class VeriDuzenle : Form
    {
      
        string videoUrl = null;
        int labelWith = 250,textWith=95,left=50,top=60;
        Object firstObj = new Object();
        bool veriPage = true;
        Surec surec = null;
        DosyaPath dosya = new DosyaPath();
        LisansInformation lisans;
        public VeriDuzenle(LisansInformation lisans)
        {
            this.lisans = lisans;
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            InitializeComponent();
            this.BackColor = Color.FromArgb(0, 0, 0, 0);
          
        }
        public void setVeriPage(bool f) {
            this.veriPage = f;
            if (veriPage)
            {
                //this.buttonBitir.Visible = true;
                this.button1.Visible = true;
                this.btnSil.Visible = false;
                this.btnVazgec.Visible = false;
            }
            else {
                this.buttonBitir.Visible = false;
                this.button1.Visible = false;
                this.btnSil.Visible = true;
                this.btnVazgec.Visible = true;
            }
        }

        public void createElement(Point location, Point location2, String txt, String value, String txtName,int kontrol,int tabIndex,string kStr,string aciklama,string[] items)
        {
            Label lbl = new Label();
            lbl.Location = location;
            lbl.Text = txt;
            lbl.Width = labelWith;
            lbl.BackColor = System.Drawing.Color.Transparent;
            lbl.Font = new Font("Times New Roman", 9, FontStyle.Bold);

            lbl.ForeColor = Color.DarkBlue ;
            if (kontrol != 3 && kontrol != 5)
            {
                TextBox txtBox = new TextBox();
                txtBox.Location = location2;
                txtBox.Text = value;
                txtBox.Font = new Font("Times New Roman", 10, FontStyle.Bold);
                txtBox.Width = textWith;
                txtBox.Height = 50;
                txtBox.TabIndex = tabIndex;
                txtBox.Name = txtName;
                txtBox.BackColor = SystemColors.Info;
                toolAciklama.SetToolTip(txtBox, aciklama);

                this.Controls.Add(txtBox);
               if (kontrol == 2)
                {
                    txtBox.Enabled = false;
                    /*    Button key = new Button();
                       key.Name = "btnEnb" + txtName;
                       key.Text = "";
                       key.Width = 25;
                       key.Height = 25;
                       key.TabIndex = 200 + tabIndex;
                       key.Location = new Point(location.X + 225, location2.Y - 1);
                       key.BackgroundImage = global::Uygulama.Properties.Resources.locked;
                       key.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
                       key.FlatAppearance.BorderSize = 0;
                       key.Click += new System.EventHandler(this.enabledYap);
                       this.Controls.Add(key);
                     else*/
                }

                if (kontrol == 4)
                {
                    comboItem_change(this.Controls[kStr] as ComboBox, null);
                }
                if (tabIndex == 1)
                    this.firstObj = txtBox;

            }
            else if (kontrol == 3)
            {
                Button yeni = new Button();
                yeni.Text = "+";
                yeni.Name = "btn"+txtName;
                yeni.Location = new Point(location2.X - 35, location.Y-5);
                yeni.Width = 25;
                yeni.Click+= new System.EventHandler(this.yeniDegerEkle);
                ComboBox cb = new ComboBox();
                cb.Location = location2;
                cb.Name = txtName;
                cb.Width = textWith;
                cb.Height = 50;
                cb.TabIndex = tabIndex;
                toolAciklama.SetToolTip(cb, aciklama);
                cb.BackColor = SystemColors.Info;
                if (kStr != null)
                {
                    comboItemSetle(cb, kStr, value);
                    cb.SelectedIndexChanged += new System.EventHandler(this.comboItem_change);
                }

                this.Controls.Add(yeni);
                this.Controls.Add(cb);
                if (tabIndex == 1)
                    this.firstObj = cb;

            }
            else if (kontrol == 5)
            {
                ComboBox cb = new ComboBox();
                cb.Location = new Point(location2.X - 100, location2.Y); ;
                cb.Name = txtName;
                cb.Width = textWith+100;
                cb.Height = 50;
                cb.TabIndex = tabIndex;
                toolAciklama.SetToolTip(cb, aciklama);
                cb.BackColor = SystemColors.Info;
                if (items != null)
                {
                    comboKonfigurasyonItemSetle(cb, items);
                    cb.SelectedIndexChanged += new System.EventHandler(this.comboItemKongigure_change);
                }


                this.Controls.Add(cb);
                if (tabIndex == 1)
                    this.firstObj = cb;
            }
            Button resimBtn = new Button();
            resimBtn.Location=new Point(location2.X + 101, location2.Y-3);
            resimBtn.Name = "btn" + txtName;
            resimBtn.Text = "";
            resimBtn.Width = 30;
            resimBtn.Height = 30;
            resimBtn.BackgroundImage = global::Uygulama.Properties.Resources.camera;
            resimBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            resimBtn.Click += new System.EventHandler(this.resimAc);
            resimBtn.FlatAppearance.BorderSize = 0;
            resimBtn.BackColor= System.Drawing.Color.Transparent;
            resimBtn.TabIndex = 100 + tabIndex;
  
            this.Controls.Add(resimBtn);
            this.Controls.Add(lbl);
            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (lisans.getGecerli())
                if (videoUrl != null)
                {
                    Process.Start(new ProcessStartInfo(videoUrl) { UseShellExecute = true });
                }
                else
                    ((YeniDizayn)this.ParentForm).Alert("Bu Sürece Ait Eğitim Videosu Eklenmemiştir.", Form_Alert.enmType.Info);
            else
                ((YeniDizayn)this.ParentForm).alertLisans();


        }
        public void setVideoUrl(string url) {
            this.videoUrl = url;
        }

        private void btnKaydet_Click(object sender, EventArgs e)
        {
            YeniDizayn tr = (YeniDizayn)this.ParentForm;
            if (lisans.getGecerli())
            {

                if (veriPage)
                {

                    tr.panelVeriAlKaydet();
                }
                else
                {
                    tr.degerKaydet(verileriEkrandanAl(dosya.txtVeriOku(surec.getSurec())), false);
                }
            }
            else
                tr.alertLisans();


        }

        private void resimAc(object sender, EventArgs e) {
            Button btn = (Button)sender;
            ResimGoster rsm = new ResimGoster();
            rsm.resimAc(btn.Name.Substring(3),0);
            rsm.StartPosition = FormStartPosition.Manual;
            rsm.Location = btn.Location;
            rsm.Show();
        }

        private void enabledYap(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
        
            TextBox textBox=this.Controls[btn.Name.Substring(6)] as TextBox;
            if(textBox.Enabled)
            {

                btn.BackgroundImage = global::Uygulama.Properties.Resources.locked;
                textBox.Enabled = !textBox.Enabled;
            }
            else
            {
                textBox.Enabled = !textBox.Enabled;
                btn.BackgroundImage = global::Uygulama.Properties.Resources._lock;
            }
           
          
        }

        private void yeniDegerEkle(object sender, EventArgs e)
        {
            YeniDizayn tr = (YeniDizayn)this.ParentForm;
            tr.panelVeriEkle(surec.getSurec(), surec.getSurecGorunmeyecek(), surec.getVideoUrlLink(surec.getSurecListName()), false);


        }


        private void comboItem_change(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            ComboBoxItem combo = (ComboBoxItem)cb.SelectedItem;
             if (combo.Map != null)

                foreach (KeyValuePair<string, string> entry in combo.Map)
                {
                    TextBox txt = this.Controls[entry.Key.Trim()] as TextBox;
                    if (txt != null && combo.GetMapValue(entry.Key.Trim()) != null)
                        txt.Text = entry.Value.Trim();
                   
                }
               
            

        }

        private void comboItemKongigure_change(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            ComboBoxItem combo = (ComboBoxItem)cb.SelectedItem;
            if (combo.Value != null && !combo.Label.Equals("Hiçbiri"))
            {
                string[] values = dosya.txtVeriOku(Environment.CurrentDirectory + "\\Süreç\\" + this.surec.getAnaSurecUrl() + "\\" + combo.Value);
                foreach (string v in values)
                {
                    string[] c = v.Split("=");
                    TextBox txt = this.Controls[c[0].Replace("\"", "").Trim()] as TextBox;
                    if (txt != null) { 
                        txt.Text = c[1].TrimStart().TrimEnd();
                        txt.Enabled = false;
                    }
                }
            }
            else {
                string[] values = dosya.txtVeriOku(this.surec.getSurec());
                foreach (string v in values)
                {
                    string[] c = v.Split("=");
                    TextBox txt = this.Controls[c[0].Replace("\"", "").Trim()] as TextBox;
                    if (txt != null) { 
                        txt.Text = c[1].TrimStart().TrimEnd();
                        txt.Enabled = true;
                    }
                }
            }
           







        }



        private void comboItemSetle(ComboBox cb,string kStr,string value)
        {
             int selectedIndex =0;
            kStr = kStr.Replace("{","").Replace("}","");
            if (!kStr.Trim().Equals(""))
            {
                string[] items = kStr.Trim().Split(",");
        
           
            for (int i=0;i<items.Length;i++) {
                if (items.Length>1 && items[i].Contains("-"))
                {
                    string[] str = items[i].Split(";");
                    string[] maps = str[1].Split("-");
                    Dictionary<string,string> map= new Dictionary<string, string>();
                    if (maps.Length > 1)
                        for (int j = 0; j < maps.Length; j++)
                        {
                            string[] v = maps[j].Split(":");
                            map.Add(v[0].TrimStart().TrimEnd(),v[1]);
                        }
                    cb.Items.Add(new ComboBoxItem(items[i].Split(";")[0], items[i].Split(";")[0],map));

                    if (str[0] == value)
                        selectedIndex = i;
                }
                else
                { 
                 
                     cb.Items.Add(new ComboBoxItem(items[i].Split(";")[0], items[i].Split(";")[1]));
                            
                        if (items[i].Split(";")[1] == value)
                          selectedIndex = i;
                 
                }
            }

            cb.SelectedIndex = selectedIndex;
            }
        }


        private void comboKonfigurasyonItemSetle(ComboBox cb, string[] kStr)
        {
           


            cb.Items.Add(new ComboBoxItem("Hiçbiri", "0"));
            foreach (string k in kStr)
            {
                
                string[] items = k.Trim().Split("\\");
                cb.Items.Add(new ComboBoxItem(items[1].Split(".")[0],k));
            }

       

        }
        
        private void buttonBitir_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            btn.Enabled = false;
            try
            {
                YeniDizayn tr = (YeniDizayn)this.ParentForm;
                tr.sureciSonlandir();
            }
            catch {
                btn.Enabled = true;
            }
            btn.Enabled = true;
        }

        private void btnSil_Click(object sender, EventArgs e)
        {
            YeniDizayn tr = (YeniDizayn)this.ParentForm;
            tr.degerKaydet(verileriEkrandanAl(dosya.txtVeriOku(surec.getSurec())), true);
        }

        private void btnVazgec_Click(object sender, EventArgs e)
        {
            YeniDizayn tr = (YeniDizayn)this.ParentForm;
            tr.panelVeriEkle(surec.getSurec(), surec.getSurecGorunmeyecek(), surec.getVideoUrlLink(surec.getSurecListName()), true);
        }

        public void verileriDosyadanAl(string[] arrLine,string[] kontrolLine,Surec src) {
            //this.Controls.Clear();
            this.surec = src;
            List<string> items=new List<string>();
            if (arrLine != null && arrLine.Length > 0)
            {
                int i = 0;
                int count = 0;
                int x = 0;
                int y = 1;
                int kontrol = 0;
                int tabIndex = 1;
                string kStr = null;
                while (i < arrLine.Length)
                {
                    string aStr = "Açıklama Bulunamadı.";
                    string[] Suspansiyon = arrLine[i].Split('=');
                    string name = "", value = "", label = "";
                    if (Suspansiyon[0].Split(";").Length > 1)
                    {
                        name = Suspansiyon[0].Split(";")[0];
                        label = Suspansiyon[0].Split(";")[1];

                    }
                    else {
                        name = Suspansiyon[0];
                        label = name;
                    }
                    value = Suspansiyon[1];
                    foreach(string d in kontrolLine){
                        string[] v = d.Split(";");

                        if (v[0].Replace("\"", "").Trim().Equals(name.Replace("\"", "").Trim()))
                            if (v.Length == 1)
                                kontrol = 1;
                            else if (v.Length > 1 && v[1] == "2")
                            {
                                kontrol = 2;
                            }
                            else if (v.Length > 1 && d.Contains("="))
                            {
                                kontrol = 4;
                                kStr = d.Split(";")[1].Split("=")[0].TrimEnd().TrimStart().Replace("\"","");
                            }
                       
                    }
                    if (src.getDegerler() != null&& veriPage)
                        foreach (string cL in src.getDegerler())
                        {
                            string[] c = cL.Split("=");
                            if (c[0].Replace("\"", "").Trim().Equals(name.Replace("\"", "").Trim()))
                            {
                                kontrol = 3;
                                kStr = c[1];
                            }
                        }
                    if (src.getKonfigurasyonlar() != null && veriPage)
                        foreach (string cL in src.getKonfigurasyonlar())
                        {
                            string[] c = cL.Split("=");
                            if (c[0].Replace("\"", "").Trim().Equals(name.Replace("\"", "").Trim()))
                            {
                                kontrol = 5;
                                items.Add(c[1]);
                            }
                        }
                    if (src.getAciklamalar()!= null)
                    foreach (string aL in src.getAciklamalar())
                    {
                        string[] c = aL.Split(";");
                        if (c[0].Replace("\"", "").Trim().Equals(name.Replace("\"", "").Trim()))
                        {
                            aStr = c[1].Replace("\"", "").TrimEnd().TrimStart();
                        }
                    }
                        if (kontrol !=1) {
                        if (count % 2 == 0)
                        {
                            x = 10;
                            y += top;
                        }
                        createElement(new Point(x, y), new Point(x + labelWith, y - 5), label.Replace("\"", "").Trim(), value.TrimStart().TrimEnd(), name.TrimStart().Replace("\"", "").TrimEnd(),kontrol, tabIndex++,kStr,aStr,items.ToArray());
                        x = x + textWith + labelWith + left;
                        count++ ;
                    }
                    kontrol = 0;

                    i++;
                }
            }
            else
                throw new Exception("Veri Bulunamadı.");
            
            if(firstObj.GetType().Name=="TextBox")
            this.ActiveControl = this.firstObj as TextBox;
            else if(firstObj.GetType().Name == "ComboBox")
                this.ActiveControl = this.firstObj as ComboBox;
        }


        public string[] verileriEkrandanAl(string[] arrLine) {
           
            int i = 0;

            while (i < arrLine.Length)
            {



                string[] Suspansiyon = arrLine[i].Split('=');
                string name = "",  label = "";
                if (Suspansiyon[0].Split(";").Length > 1)
                {
                    name = Suspansiyon[0].Split(";")[0];
                    label = Suspansiyon[0].Split(";")[1];

                }
                else
                {
                    name = Suspansiyon[0];
                    label = name;
                }
                if (this.Controls[name.TrimStart().TrimEnd().Replace("\"", "")]!=null && this.Controls[name.TrimStart().TrimEnd().Replace("\"", "")].GetType().Name == "TextBox")
                {
                    TextBox tx = this.Controls[name.TrimStart().TrimEnd().Replace("\"", "")] as TextBox;
                    if (tx != null)
                        arrLine[i] = Suspansiyon[0] + " = " + tx.Text;
                }
                else if (this.Controls[name.TrimStart().TrimEnd().Replace("\"", "")]!=null && this.Controls[name.TrimStart().TrimEnd().Replace("\"", "")].GetType().Name=="ComboBox")
                {
                    ComboBox cb = this.Controls[name.TrimStart().TrimEnd().Replace("\"", "")] as ComboBox;
                    if (cb != null)
                        arrLine[i] = Suspansiyon[0] + " = " + ((ComboBoxItem) cb.SelectedItem).Value;
                }
                i++;

            }
            return arrLine;

            

        }
        public void setLabelBaslik(string label) {
            this.labelBaslik.Text = label;
           // this.buttonBitir.Text=label + " Sürecini Solide Aktar";
        }


    
    }
}
