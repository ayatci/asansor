﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uygulama
{
    public class Konfigurasyonlar
    {
        string kodu;
        string ustKodu;
        string adi;

        public Konfigurasyonlar()
        {
        
        }
        public void setKodu(string kodu) {
            this.kodu = kodu;
        }
        public void setUstKodu(string ustKodu)
        {
            this.ustKodu =ustKodu;
        }

        public void setAdi(string adi)
        {
            this.adi = adi;
        }

        public string getKodu()
        {
            return this.kodu;
        }
        public string getUstKodu()
        {
           return this.ustKodu;
        }

        public string getAdi()
        {
            return this.adi;
        }

  



    }
}
