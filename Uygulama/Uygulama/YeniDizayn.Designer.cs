﻿
namespace Uygulama
{
    partial class YeniDizayn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(YeniDizayn));
            this.panelSideMenu = new System.Windows.Forms.Panel();
            this.btnHakkinda = new System.Windows.Forms.Button();
            this.btnSolideAktar = new System.Windows.Forms.Button();
            this.btnAyarlar = new System.Windows.Forms.Button();
            this.panelSideMenu2 = new System.Windows.Forms.Panel();
            this.panelKonf = new System.Windows.Forms.Panel();
            this.btnDwgAc = new System.Windows.Forms.Button();
            this.panelLogo = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelVeri = new System.Windows.Forms.Panel();
            this.panelSideMenu.SuspendLayout();
            this.panelSideMenu2.SuspendLayout();
            this.panelLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelSideMenu
            // 
            this.panelSideMenu.AutoScroll = true;
            this.panelSideMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.panelSideMenu.Controls.Add(this.btnHakkinda);
            this.panelSideMenu.Controls.Add(this.btnSolideAktar);
            this.panelSideMenu.Controls.Add(this.btnAyarlar);
            this.panelSideMenu.Controls.Add(this.panelSideMenu2);
            this.panelSideMenu.Controls.Add(this.btnDwgAc);
            this.panelSideMenu.Controls.Add(this.panelLogo);
            this.panelSideMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelSideMenu.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.panelSideMenu.Location = new System.Drawing.Point(0, 0);
            this.panelSideMenu.Name = "panelSideMenu";
            this.panelSideMenu.Size = new System.Drawing.Size(300, 711);
            this.panelSideMenu.TabIndex = 0;
            // 
            // btnHakkinda
            // 
            this.btnHakkinda.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnHakkinda.FlatAppearance.BorderSize = 0;
            this.btnHakkinda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHakkinda.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnHakkinda.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.btnHakkinda.Location = new System.Drawing.Point(0, 228);
            this.btnHakkinda.Name = "btnHakkinda";
            this.btnHakkinda.Padding = new System.Windows.Forms.Padding(1, 0, 0, 0);
            this.btnHakkinda.Size = new System.Drawing.Size(300, 45);
            this.btnHakkinda.TabIndex = 9;
            this.btnHakkinda.Text = "HAKKINDA";
            this.btnHakkinda.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHakkinda.UseVisualStyleBackColor = true;
            this.btnHakkinda.Click += new System.EventHandler(this.btnHakkinda_Click);
            // 
            // btnSolideAktar
            // 
            this.btnSolideAktar.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSolideAktar.FlatAppearance.BorderSize = 0;
            this.btnSolideAktar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSolideAktar.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnSolideAktar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.btnSolideAktar.Location = new System.Drawing.Point(0, 183);
            this.btnSolideAktar.Name = "btnSolideAktar";
            this.btnSolideAktar.Padding = new System.Windows.Forms.Padding(1, 0, 0, 0);
            this.btnSolideAktar.Size = new System.Drawing.Size(300, 45);
            this.btnSolideAktar.TabIndex = 6;
            this.btnSolideAktar.Text = "TÜM SÜREÇLERİ SOLİDWORKS\'E AKTAR";
            this.btnSolideAktar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSolideAktar.UseVisualStyleBackColor = true;
            this.btnSolideAktar.Click += new System.EventHandler(this.btnSolide_Butun_Degerleri_Aktar_Click);
            // 
            // btnAyarlar
            // 
            this.btnAyarlar.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAyarlar.FlatAppearance.BorderSize = 0;
            this.btnAyarlar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAyarlar.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnAyarlar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.btnAyarlar.Location = new System.Drawing.Point(0, 138);
            this.btnAyarlar.Name = "btnAyarlar";
            this.btnAyarlar.Padding = new System.Windows.Forms.Padding(1, 0, 0, 0);
            this.btnAyarlar.Size = new System.Drawing.Size(300, 45);
            this.btnAyarlar.TabIndex = 7;
            this.btnAyarlar.Text = "AYARLAR";
            this.btnAyarlar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAyarlar.UseVisualStyleBackColor = true;
            this.btnAyarlar.Click += new System.EventHandler(this.btnAyarlar_Click);
            // 
            // panelSideMenu2
            // 
            this.panelSideMenu2.AutoSize = true;
            this.panelSideMenu2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelSideMenu2.Controls.Add(this.panelKonf);
            this.panelSideMenu2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSideMenu2.Location = new System.Drawing.Point(0, 138);
            this.panelSideMenu2.Name = "panelSideMenu2";
            this.panelSideMenu2.Size = new System.Drawing.Size(300, 0);
            this.panelSideMenu2.TabIndex = 6;
            // 
            // panelKonf
            // 
            this.panelKonf.AutoSize = true;
            this.panelKonf.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelKonf.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelKonf.Location = new System.Drawing.Point(0, 0);
            this.panelKonf.Name = "panelKonf";
            this.panelKonf.Size = new System.Drawing.Size(300, 0);
            this.panelKonf.TabIndex = 5;
            // 
            // btnDwgAc
            // 
            this.btnDwgAc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.btnDwgAc.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnDwgAc.FlatAppearance.BorderSize = 0;
            this.btnDwgAc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDwgAc.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnDwgAc.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.btnDwgAc.Location = new System.Drawing.Point(0, 93);
            this.btnDwgAc.Name = "btnDwgAc";
            this.btnDwgAc.Padding = new System.Windows.Forms.Padding(1, 0, 0, 0);
            this.btnDwgAc.Size = new System.Drawing.Size(300, 45);
            this.btnDwgAc.TabIndex = 1;
            this.btnDwgAc.Text = "ÇİZİM UYGULAMASINI AÇ";
            this.btnDwgAc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDwgAc.UseVisualStyleBackColor = false;
            this.btnDwgAc.Click += new System.EventHandler(this.btnDwg_Click);
            // 
            // panelLogo
            // 
            this.panelLogo.Controls.Add(this.pictureBox1);
            this.panelLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLogo.Location = new System.Drawing.Point(0, 0);
            this.panelLogo.Name = "panelLogo";
            this.panelLogo.Size = new System.Drawing.Size(300, 93);
            this.panelLogo.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(300, 93);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panelVeri
            // 
            this.panelVeri.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panelVeri.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelVeri.Location = new System.Drawing.Point(300, 0);
            this.panelVeri.Name = "panelVeri";
            this.panelVeri.Size = new System.Drawing.Size(884, 711);
            this.panelVeri.TabIndex = 1;
            // 
            // YeniDizayn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1184, 711);
            this.Controls.Add(this.panelVeri);
            this.Controls.Add(this.panelSideMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1100, 650);
            this.Name = "YeniDizayn";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Asa-Lift Asansör Çizim Programı";
            this.panelSideMenu.ResumeLayout(false);
            this.panelSideMenu.PerformLayout();
            this.panelSideMenu2.ResumeLayout(false);
            this.panelSideMenu2.PerformLayout();
            this.panelLogo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelSideMenu;
        private System.Windows.Forms.Button btnDwgAc;
        private System.Windows.Forms.Panel panelLogo;
        private System.Windows.Forms.Panel panelVeri;
        private System.Windows.Forms.Panel panelSideMenu2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnAyarlar;
        private System.Windows.Forms.Panel panelKonf;
        private System.Windows.Forms.Button btnSolideAktar;
        private System.Windows.Forms.Button btnHakkinda;
    }
}