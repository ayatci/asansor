﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uygulama
{
    public class LisansInformation
    {
        
        string lisansMusteri;
        DateTime date;
        bool gecerli;
        public void setLisansMusteri(string musteri) {
            this.lisansMusteri = musteri;
        }
        public string getLisansMusteri()
        {
            return this.lisansMusteri;
        }
        public void setTarih(DateTime tarih)
        {
            this.date = tarih;
        }
        public DateTime getTarih()
        {
            return this.date;
        }
        public void setGecerli(bool gecerli)
        {
            this.gecerli = gecerli;
        }
        public bool getGecerli()
        {
            return this.gecerli;

    }
    }
}
