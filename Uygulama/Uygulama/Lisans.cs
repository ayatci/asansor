﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security.Cryptography;

namespace Uygulama
{
    class Lisans
    {
        string source = "";
        string remote = "";

        public static byte[] Encrpyt(byte[] data, byte[] key, byte[] iv)
        {
            using (Aes algorithm = Aes.Create())
            using (ICryptoTransform encryptor = algorithm.CreateEncryptor(key, iv))
            {
                return Crypt(data, key, iv, encryptor);
            }
        }
        public static byte[] Decrpyt(byte[] data, byte[] key, byte[] iv)
        {
            using (Aes algorithm = Aes.Create())
            using (ICryptoTransform decryptor = algorithm.CreateDecryptor(key, iv))
            {
                return Crypt(data, key, iv, decryptor);
            }
        }

        public string Encrpyt(string data, byte[] key, byte[] iv)
        {
            if (data == null)
                return null;
            return Convert.ToBase64String(
              Encrpyt(Encoding.UTF8.GetBytes(data), key, iv));
        }

        public string Decrpyt(string data, byte[] key, byte[] iv)
        {
            try
            {
                if (data == null)
                    return null;

                return Encoding.UTF8.GetString(
                Decrpyt(Convert.FromBase64String(data), key, iv));
            }
            catch {
                return null;
            }
        }
        private static byte[] Crypt(byte[] data, byte[] key, byte[] iv, ICryptoTransform cryptor)
        {
            MemoryStream m = new MemoryStream();
            using (Stream c = new CryptoStream(m, cryptor, CryptoStreamMode.Write))
            {
                c.Write(data, 0, data.Length);
            }
            return m.ToArray();
        }

        public bool lisansKontrol() {
            if (source.Equals(remote))
                return true;
            else return false;
        }
    }
}
