﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uygulama
{
    public class ComboBoxItem
    {
        public string Label;
        public string Value;
        public Dictionary<string, string> Map;
        public ComboBoxItem(string Label, string Value)
        {
            this.Label = Label;
            this.Value = Value;
        }
      
        public ComboBoxItem(string Label, string Value,Dictionary<string,string> Map)
        {
            this.Label = Label;
            this.Value = Value;
            this.Map = Map;
        }
        public override string ToString()
        {
            return this.Label;
        }
        public  string GetMapValue(string key)
        {
            try
            {

                return this.Map[key.Replace("\"","")];
            }
            catch
            {
                return null;
            }
            
        }
    }
}

