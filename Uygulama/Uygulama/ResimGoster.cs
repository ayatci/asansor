﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Uygulama
{
    public partial class ResimGoster : Form
    {
        public ResimGoster()
        {
            InitializeComponent();
        }
        string[] kisaltma = {".jpg",".jpeg",".png" };

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void resimAc(string url,int val)
        {
            if (url != null)
                try
                {
                   
                  
                    this.pictureBox.Image = Image.FromFile(System.IO.Path.Combine(Environment.CurrentDirectory, @"Resimler\", url + kisaltma[val]));
                }
                catch {
                    if(val<3)
                    resimAc(url, val + 1);
                }
            else
                this.pictureBox.Image = global::Uygulama.Properties.Resources.no_image;

        }
    }
}
