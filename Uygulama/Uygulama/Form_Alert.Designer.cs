﻿
namespace Uygulama
{
    partial class Form_Alert
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblMsg = new System.Windows.Forms.Label();
            this.btnAlertClose = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblMsg
            // 
            this.lblMsg.AutoSize = true;
            this.lblMsg.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblMsg.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblMsg.Location = new System.Drawing.Point(36, 19);
            this.lblMsg.MaximumSize = new System.Drawing.Size(300, 90);
            this.lblMsg.Name = "lblMsg";
            this.lblMsg.Size = new System.Drawing.Size(124, 14);
            this.lblMsg.TabIndex = 0;
            this.lblMsg.Text = "KAYDETME BAŞARILI";
            // 
            // btnAlertClose
            // 
            this.btnAlertClose.BackColor = System.Drawing.Color.Red;
            this.btnAlertClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnAlertClose.FlatAppearance.BorderColor = System.Drawing.SystemColors.WindowFrame;
            this.btnAlertClose.FlatAppearance.BorderSize = 0;
            this.btnAlertClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlertClose.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnAlertClose.ForeColor = System.Drawing.Color.White;
            this.btnAlertClose.Location = new System.Drawing.Point(375, -1);
            this.btnAlertClose.Name = "btnAlertClose";
            this.btnAlertClose.Size = new System.Drawing.Size(25, 25);
            this.btnAlertClose.TabIndex = 1;
            this.btnAlertClose.Text = "X";
            this.btnAlertClose.UseVisualStyleBackColor = false;
            this.btnAlertClose.Click += new System.EventHandler(this.btnAlertClose_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Uygulama.Properties.Resources.icons8_checkmark_48;
            this.pictureBox1.Location = new System.Drawing.Point(0, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(25, 25);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form_Alert
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.SeaGreen;
            this.ClientSize = new System.Drawing.Size(400, 60);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnAlertClose);
            this.Controls.Add(this.lblMsg);
            this.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MinimumSize = new System.Drawing.Size(400, 60);
            this.Name = "Form_Alert";
            this.Text = "Form_Alert";
            this.Load += new System.EventHandler(this.Form_Alert_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMsg;
        private System.Windows.Forms.Button btnAlertClose;
        private System.Windows.Forms.PictureBox pictureBox1;
        internal System.Windows.Forms.Timer timer1;
    }
}