﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;



namespace Uygulama
{
    public partial class Ayarlar : Form
    {
        DosyaPath dosya = new DosyaPath();
        LisansInformation lisans;
        string[] mList = { };
        string[] cList = { };
        string[] kList = { };
       
        public Ayarlar(LisansInformation lisans)
        {
            
            this.lisans = lisans;
            InitializeComponent();
        }
        private void Ayarlar_Load(object sender, EventArgs e)
        {
            

            menuSonucDosyaYoluSec();

        }
        public void menuSonucDosyaYoluSec() {
            int y = 40;
           YeniDizayn tr = (YeniDizayn)this.ParentForm;
            if (lisans.getGecerli())
            {
                mList = dosya.txtVeriOku(dosya.getMenuPath());

                foreach (string m in mList)
                {
                    string[] t = m.Split("=");
                    // OpenFileDialog fileDialog = new OpenFileDialog();
                    //  fileDialog.FileName = t[1];
                    y = this.createGozat(t[1].Trim(), t[0].Trim() + " Sonucunda Oluşan Dosyanın Yazılacağı Yolu Seçiniz.", t.Length > 2 ? t[2].Trim() : null, 500, y,true);

                }
                //konfigurasyonVerileriniSec(310, y);

            }
            else
                tr.alertLisans();
            propertiesVerileriniSec(310, y);

        }
     
        public void propertiesVerileriniSec(int x,int y) {
            cList = dosya.txtVeriOku(dosya.getConfigPath());
            foreach (string m in cList)
            {
                string[] k = m.Split("-");
                if(k[0].Trim() =="0")
                { 
                string[] t = k[1].Split("=");
                    // OpenFileDialog fileDialog = new OpenFileDialog();
                    //  fileDialog.FileName = t[1];
                    y=this.createGozat(t[0].Trim()+"Config", t[0].Trim() + " Seçiniz..", t.Length > 1 ? t[1].Trim():null,500,y,false) ;
                }
            }
            kaydet.Location = new Point(600, y);

        }

        public void konfigurasyonVerileriniSec(int x, int y)
        {
            kList = dosya.txtVeriOku(dosya.getKonfPath());
            foreach (string m in kList)
            {
                string[] t = m.Split("=");
                // OpenFileDialog fileDialog = new OpenFileDialog();
                //  fileDialog.FileName = t[1];
                y = this.createGozat(t[1].Trim()+"Konf", t[0].Trim() + " Sonucunda Oluşan Dosyanın Yazılacağı Yolu Seçiniz.", t.Length > 3 ? t[3].Trim() : null, 500, y,false);

            }
           // propertiesVerileriniSec(310, y);
            
        }
        public void menuGozat(object sender, EventArgs e) {
            Button btn = (sender as Button);
            TextBox tx = this.Controls[btn.Name] as TextBox;
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            openFileDialog1.InitialDirectory = @"C:\";
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.Title = "Browse Text Files";
            openFileDialog1.DefaultExt = "txt";
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.CheckFileExists = true;
            openFileDialog1.CheckPathExists = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                
                tx.Text = openFileDialog1.FileName;
            }
        }
        public void menuGozatDirectory(object sender, EventArgs e)
        {
            Button btn = (sender as Button);
            TextBox tx = this.Controls[btn.Name] as TextBox;
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();

            //openFileDialog1.DefaultExt = "txt";
            // openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";


            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {

                tx.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private Int32 createGozat(string name, string label, string text, Int32 size, Int32 y,bool check)
        {

            Label lbl = new Label();
            lbl.Text = label;
            lbl.Location = new Point(5, y);
            lbl.Size = new Size(size, 15);
            TextBox txt = new TextBox();
            txt.Name = name;
            txt.Size = new Size(size, 23);
            txt.BackColor = SystemColors.Control;
            txt.Enabled = false;
            txt.Location = new Point(5, y + 20);
            txt.Text = text;
            Button btn = new Button();
            btn.Name = name;
            btn.Size = new Size(93, 32);
            btn.Text = "Gözat";
            btn.Location = new Point(size + 10, y + 15);
            if(check)
                btn.Click += new System.EventHandler(menuGozatDirectory);
            else
            btn.Click += new System.EventHandler( menuGozat );
            y = y + 60;
            this.Controls.Add(lbl);
            this.Controls.Add(txt);
            this.Controls.Add(btn);
            return y;
        }



        private void kaydet_Click(object sender, EventArgs e)
        {
           
            try
            {
                if (lisans.getGecerli())
                {

                    for (int i = 0; i < mList.Length; i++)
                    {
                        string[] t = mList[i].Split("=");
                        TextBox tx = this.Controls[t[1].Trim()] as TextBox;
                        if (tx.Text != null)
                            mList[i] = t[0].Trim() + " = " + t[1].Trim() + " = " + tx.Text;


                    }
                    dosya.txtVeriYaz(dosya.getMenuPath(), mList);
                    for (int i = 0; i < kList.Length; i++)
                    {
                        string[] t = kList[i].Split("=");
                        TextBox tx = this.Controls[t[1].Trim() + "Konf"] as TextBox;
                        if (tx.Text != null)
                            kList[i] = t[0].Trim() + " = " + t[1].Trim() + " = " + t[2].Trim() + " = " + tx.Text;


                    }
                    dosya.txtVeriYaz(dosya.getKonfPath(), kList);
                   
                }
                for (int i = 0; i < cList.Length; i++)
                {
                    string[] k = cList[i].Split("-");
                    if (k[0].Trim() == "0")
                    {
                        string[] t = k[1].Split("=");
                        TextBox tx = this.Controls[t[0].Trim() + "Config"] as TextBox;
                        if (tx.Text != null)
                            cList[i] = "0 - " + t[0].Trim() + " = " + tx.Text;

                    }
                }
                dosya.txtVeriYaz(dosya.getConfigPath(), cList);
                // this.Alert("Kayıt İşlemi Başarıyla Gerçekleşti",Form_Alert.enmType.Success);
            }
            catch (Exception msg) {
                this.Alert(msg.Message, Form_Alert.enmType.Error);
            }
            
        }

  
        public void Alert(string msg, Form_Alert.enmType type)
        {
            Form_Alert frm = new Form_Alert(); 
            frm.showAlert(msg, type);
        }

   /*     private void button2_Click(object sender, EventArgs e)
        {
            SldWorks swApp = new SldWorks();
            ModelDoc2 mDoc;
            PartDoc swPart;
            ModelDoc2 swModel;
            object[] dataAlignment = {0,0,0,1,0,0,1,0,0,0,1 } ;
            object[] dataViews = { "*Current", "*Front" };


            mDoc = (ModelDoc2)swApp.GetFirstDocument();


            int i = 0;
            while (!(mDoc is null))
            {
                String aa = mDoc.GetPathName();
                if (mDoc.GetType() == 1)
                {
                    swModel = swApp.GetOpenDocument(aa);
                    swPart = (PartDoc)swModel;
                    bool a=swPart.ExportToDWG2("D:\\SolidÇizim\\son\\seyit\\kabin süspansiyonu\\Deneme", "deneme_" + i, (int)swExportToDWG_e.swExportToDWG_ExportAnnotationViews, false, dataAlignment,
                        false, false, 0, dataViews);
                   a= swPart.ExportToDWG2("D:\\SolidÇizim\\son\\seyit\\kabin süspansiyonu\\Deneme", "deneme_" + i,(int) swExportToDWG_e.swExportToDWG_ExportSheetMetal, true, dataAlignment,
                        false, false, 1, null);
                    i = i + 1;
                    swApp.CloseDoc(aa);
                }
                mDoc = (ModelDoc2) mDoc.GetNext();
            
            }


        }*/
    }
}
