﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Uygulama
{
    public partial class Konfigurasyon : Form
    {

        List<Konfigurasyonlar> konfList;
        int count = 0;
        int level = 0;
        DosyaPath dosyaPath = new DosyaPath();
        string surecName = "";
        string[] veris;
       Point location = new Point(-150, 50);
        
        
        public Konfigurasyon()
        {
            InitializeComponent();
         
        }
        public void setKonfigurasyonList(List<Konfigurasyonlar> list) {
            this.konfList = list;
        }
        public void setSurecName(string name)
        {
            this.surecName = name;
        }
        public void setVeris()
        {
            this.veris=dosyaPath.txtVeriOku(dosyaPath.getKonfSurecPath(this.surecName));
            if (veris.Length > 0)
            {
                foreach (string item in veris[0].Split(";"))
                { 
                    ComboBox cb= this.Controls[(level-1).ToString() + "-" + (count-1).ToString()] as ComboBox;
                    if (cb != null)
                    {
                        foreach (ComboBoxItem cbi in cb.Items)
                        {
                            if (cbi.Value.Equals(item))
                            {
                                cb.SelectedItem = cbi;
                                break;
                            }
                        }

                        
                        comboItem_change(cb, null);
                       // count++;

                    }
                }
                this.seciliVerileriYaz();


            }
        }


        public void verileriOkuSetle() {
            List<ComboBoxItem> list = findByUstKoduComboItems(konfList, "");
            if (list.Count > 0)
            {
                location.Y = location.Y + 50;

                createCombo(list, level.ToString() + "-" + count.ToString(), location);
                level++;
                count++;
            }
        }

        public List<ComboBoxItem> findByUstKoduComboItems(List<Konfigurasyonlar> list, string ustKodu)
        {
            List<ComboBoxItem> yeniList = new List<ComboBoxItem>();
            foreach (Konfigurasyonlar l in list)
                if (l.getUstKodu().Equals(ustKodu))
                    yeniList.Add(new ComboBoxItem(l.getAdi(), l.getKodu()));



            return yeniList;
        }

        private void comboItem_change(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            int seviye =Int16.Parse(cb.Name.Split("-")[1]);
            int lvl = Int16.Parse(cb.Name.Split("-")[0]);
            ComboBoxItem combo = (ComboBoxItem)cb.SelectedItem;
            List<ComboBoxItem> list = findByUstKoduComboItems(konfList, combo.Value);

            if (list.Count > 0 && (count - seviye) == 1)
            {
                createCombo(list, lvl.ToString() + "-" + count.ToString(),cb.Location);
                count++;
            }
            else if (list.Count > 0 && (count - seviye) > 1)
            {

                for (int i = count; i - seviye > 1; i--)
                {
                    count--;
                    removeCombo(lvl.ToString() + "-"+count.ToString());

                }
                createCombo(list, lvl.ToString() + "-"+count.ToString(), cb.Location);
                count++;
            }
            else if(list.Count ==0)
            {
                for (int i = count; i - seviye > 1; i--)
                {
                    count--;
                    removeCombo(lvl.ToString() + "-" + count.ToString());

                }

            }
        
               
        


        }
        private void removeCombo(string cbName) {
            ComboBox rmvCb = this.Controls[cbName] as ComboBox;
            ComboBoxItem combo = (ComboBoxItem)rmvCb.SelectedItem;
            rmvCb.SelectedIndexChanged -= new System.EventHandler(this.comboItem_change);
            this.Controls.Remove(rmvCb);
            rmvCb.Dispose();
            
           

        }

        private void createCombo(List<ComboBoxItem> list, string name,Point p) {
            ComboBox cb = new ComboBox();
            
            cb.Name = name;
            p.X = p.X + 160;
            cb.Location = p;
            cb.Width = 150;
            cb.Height = 50;
            foreach(ComboBoxItem item in list)
            cb.Items.Add(item);
            cb.SelectedIndexChanged += new System.EventHandler(this.comboItem_change);
            this.Controls.Add(cb);
        }

        private void kaydet_Click(object sender, EventArgs e)
        {
            this.seciliVerileriYaz();
            dosyaPath.txtVeriYaz(dosyaPath.getKonfSurecPath(this.surecName),label2.Text.Split("---%--%--%%") );
         

        }
        public void seciliVerileriYaz() {
            string lbl = "";
            label1.Text = "";
            label2.Text = "";
            for (int y = 0; y < level; y++)
            {
                label1.Text = label1.Text + "\r\n-";


                lbl = "";
                for (int i = 0; i < count; i++)
                {

                    ComboBox cb = this.Controls[y.ToString() + "-" + i.ToString()] as ComboBox;

                    if (cb != null)
                    {

                        ComboBoxItem combo = (ComboBoxItem)cb.SelectedItem;

                        label1.Text = label1.Text + combo.Label + ";";
                        if (!lbl.Equals(""))
                            lbl = lbl + ";" + combo.Value;
                        else
                            lbl = combo.Value;

                    }
                }
                label2.Text = lbl;
            }
        }
    }
}
