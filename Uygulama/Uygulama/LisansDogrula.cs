﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Uygulama
{
    class LisansDogrula
    {
        DosyaPath dosya = new DosyaPath();
        Lisans lisans = new Lisans();
        USBSerialNumber usb = new USBSerialNumber();
        byte[] key = Encoding.ASCII.GetBytes("ahmet123seyit456ahmet".PadRight(32).ToCharArray(), 0, 32);
        byte[] iv = Encoding.ASCII.GetBytes("asalift789".PadRight(16).ToCharArray(), 0, 16);

        public LisansInformation lisansDogrula(string urlPath) {
            LisansInformation lisansInf = new LisansInformation();
            string serial = urlPath==null||urlPath.Equals("")?null:usb.getSerialNumberFromDriveLetter(urlPath);
            string[] serialGelen = dosya.txtVeriOku(urlPath);
            string cozulmus = serialGelen.Length>0? lisans.Decrpyt(serialGelen[0], key, iv):null;
            string[] keys= cozulmus!=null?cozulmus.Split("&"):null;
            if (keys!=null &&keys.Length > 0)
            {
                string cozulmusKey = keys[0];
                lisansInf.setLisansMusteri(keys[1]);
                lisansInf.setTarih(DateTime.Parse(keys[2]));
                if (!serial.Equals(cozulmusKey))
                {
                    lisansInf.setGecerli(false);
                    if (lisansInf.getGecerli())
                        throw new Exception("Lisans Hatası");
                }
                else
                {
                    lisansInf.setGecerli(true);
                }
            }
            else
            {
                lisansInf.setGecerli(false);
                if (lisansInf.getGecerli())
                    throw new Exception("Lisans Hatası");
            }    
            return lisansInf;
        }
        public USBSerialNumber GetUSBSerialNumber() {
            return this.usb;
        }
        public Lisans GetLisans()
        {
            return this.lisans;
        }
        public byte[] getKey()
        {
            return this.key;
        }
        public byte[] getIv()
        {
            return this.iv;
        }

    }
}
